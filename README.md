Behavioural screen pipeline for UAS-Chrimson x split Gal4 lines
Developed by Yuankai He (yh464@cam.ac.uk)

Requirements:
Python environment: numpy, scipy, pandas, seaborn, matplotlib
Matlab >= R2022a (/public/matlab/matlab)

Order of scripts:

1. PLEASE EDIT ../params/drivers#XX.list and enter valid names for lines
2. /cephfs/zlatic_lab/code/cluster_scripts/choreplot.sh -t BO1 -p screen		Choreography pipeline
3. /cephfs/zlatic_lab/code/jb_pipeline/bin/extract_features_and_classify_behaviour.py --rig BO1			Jean-Baptiste Masson's pipeline
4. summary_trx_by_behav.m		Creates line plots for each behaviour across a 'batch' of lines
5. behav_align.m			Aligns behaviour across different stimulation pulses
6. behav_stats_batch.py			Chi2 test by time point and permutation test across the sample
7. plot_behav_batch.py			Plots all useful info for a line versus control

OTHER SCRIPTS ARE NOT MEANT TO BE RUN DIRECTLY, BUT ONLY FOR DEBUGGING PURPOSES
PLEASE DO NOT CHANGE THE _BATCH SCRIPTS AS THEY ARE ONLY WRAPPERS
