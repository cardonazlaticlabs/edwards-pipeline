% JB downstream plotting - comparison across genotypes

force = true; % CHANGE

for idx = 1:12
id = num2str(idx,'%02u');
f = readlines(['/cephfs/zlatic_lab/code/yhe/params/drivers#',id,'.list']);
f = f(strlength(f)>0);
addpath /cephfs/zlatic_lab/code/yhe/scripts;

behavs = ["run","cast","stop","hunch","back","roll","other"];
master = struct();
aligned = struct();
% for i = 1:length(behavs)
%     master.(behavs(i)) = struct.empty;
% end
tic;

neff = zeros(length(f),1);
nmax = zeros(length(f),1);

for i = 1:length(f)
    gen = f(i);
    disp(gen);
    disp(num2str(i) + "/" + num2str(length(f)));
    out = plot_genotype_trx(gen,force);
    for j = 1:length(behavs)
        master.(behavs(j))(i) = ...
            struct('time',out.summary.time,'behav',out.summary.("p"+behavs(j)));
        aligned.(behavs(j))(i) = ...
            struct('time',out.aligned.time,'behav',out.aligned.("p"+behavs(j)));
    end
    nts = out.summary.n;
    nmax(i) = max(nts,[],'all');
    neff(i) = mean(nts,'all');
    toc;
end

for j = 1:length(behavs)
    out_fname = "/cephfs/zlatic_lab/code/yhe/plots/"+behavs(j)+"_summary_#"+id+".pdf";
    if isfile(out_fname) && ~force; continue; end
    figure('OuterPosition',[0,0,1920,1080]);
    rectangle('Position',[30000,0,15000,1],'FaceColor','#dfdfdf');
    rectangle('Position',[75000,0,15000,1],'FaceColor','#dfdfdf');
    hold on;
    for i = 1:length(f)
        plot(master.(behavs(j))(i).time,master.(behavs(j))(i).behav);
    end
    labels = replace(f,'_','\_')+", neff="+num2str(neff,'%.2f')+", nmax="+...
        num2str(nmax, '%.0f');
    legend(labels);
    fontsize(20, 'points');
    exportgraphics(gca, out_fname);
    delete(gcf);
end

for j = 1:length(behavs)
    out_fname = "/cephfs/zlatic_lab/code/yhe/plots/"+behavs(j)+"_aligned_#"+id+".pdf";
    if isfile(out_fname) && ~force; continue; end
    figure('OuterPosition',[0,0,1920,1080]);
    rectangle('Position',[0,0,15000,1],'FaceColor','#dfdfdf');
    hold on;
    for i = 1:length(f)
        plot(aligned.(behavs(j))(i).time,aligned.(behavs(j))(i).behav);
    end
    labels = replace(f,'_','\_')+", neff="+num2str(neff,'%.2f')+", nmax="+...
        num2str(nmax, '%.0f');
    legend(labels);
    fontsize(20, 'points');
    exportgraphics(gca, out_fname);
    delete(gcf);
end

end

system('bash /cephfs/zlatic_lab/code/yhe/scripts/copyfile.sh');
cd /cephfs/zlatic_lab/code/yhe/scripts;