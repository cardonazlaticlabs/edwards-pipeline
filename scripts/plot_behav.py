#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def main(args):
  # args.ctrl
  # args.test
  # args.out = /cephfs/zlatic_lab/code/yhe/plots/ss_v_ctrl
  # args.force
  
  import os
  import pandas as pd
  import matplotlib.pyplot as plt
  import seaborn as sns
  import numpy as np
  
  if not os.path.isdir(args.out): os.system(f'mkdir -p {args.out}') # system does not recognise ~
  
  # prefixes
  pf_ctrl = os.path.basename(args.ctrl).replace('.txt','')
  pf_test = os.path.basename(args.test).replace('.txt','')
  out_fname = args.out + os.path.basename(args.test).replace('txt','pdf')
  
  if not args.force and os.path.isfile(out_fname): return 0
  
  # read data
  ctrl = pd.read_csv(args.ctrl, sep = ',') # time = col0, each subsequent column = individual
  test = pd.read_csv(args.test, sep = ',')
  neff_test = test.n.mean() / 2
  
  # plot
  ctrl['genotype'] = pf_ctrl
  test['genotype'] = pf_test
  master = pd.concat((ctrl, test))
  col = master.columns.tolist()
  col.remove('time')
  col.remove('genotype')
  nplot = len(col) + 1
  fig, ax = plt.subplots(nplot,1,sharey = False, figsize = (12,5*nplot - 8))
  for i in range(nplot - 1):
    sns.lineplot(data = master, x = 'time', y = col[i], hue = 'genotype', ax = ax[i])
    ax[i].axvspan(0, 15000, fc = '#dfdfdf', color = 'k')
    # ax[i].set_title(col[i], fontsize = 'large')
    if i < 7: ax[i].set_ylim([0,1])
  
  stat = pd.read_csv(f'{args.stat}{pf_test}.txt')
  tmp = stat[['time','gstat','sig_clusters_.05','sig_clusters_.001']]
  ax[-1].plot(tmp.time, tmp.gstat, label = 'G statistic')
  ax[-1].plot(tmp.time,tmp['sig_clusters_.05'], label = 'Sig clusters .05')
  ax[-1].plot(tmp.time,tmp['sig_clusters_.001'], label = 'Sig clusters .001')
  ax[-1].set_ylim([-5,100])
  ax[-1].set_ylabel('G statistic / chi2')
  ax[-1].set_xlabel('time (ms)')
  ax[-1].axhline(12.5916, color = 'b', linestyle = '--', label = 'chi2(p=0.95, df=6)')
  ax[-1].axhline(22.4577, color = 'r', linestyle = '--', label = 'chi2(p=0.999, df=6)')
  ax[-1].axvspan(0, 15000, fc = '#dfdfdf', color = 'k')
  ax[-1].legend()
  
  # highlight sig clusters
  tstart = np.nan
  sig = False
  for i in range(tmp.shape[0]):
    tnow = tmp.time[i]
    if not np.isnan(tmp['sig_clusters_.001'][i]):
      if not sig:
        sig = True
        tstart = tnow
    elif sig:
      sig = False
      for j in ax:
        j.axvspan(tstart, tnow, fc = '#ffffdf', color = '#ffffdf')
  if sig:
    for j in ax:
      j.axvspan(tstart, tnow, fc = '#ffffdf', color = '#ffffdf')
  
  for j in ax:
    j.axvline(0, color = 'k')
    j.axvline(15000, color = 'k')
  
  ttl = f'{pf_test} - neff = {neff_test:.2f}'
  plt.suptitle(ttl, fontsize = 'xx-large')
  plt.savefig(out_fname)
  return 0

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser(description = 'Cliffs Delta calculator for behavioural sequences')
  parser.add_argument('--ctrl', dest = 'ctrl', help = 'Control line')
  parser.add_argument('--test', dest = 'test', help = 'Test line')
  parser.add_argument('--stat', dest = 'stat', help = 'stats dir', default = '/cephfs/zlatic_lab/code/yhe/stats/')
  parser.add_argument('-o','--out', dest = 'out', help = 'Output dir', default = '/cephfs/zlatic_lab/code/yhe/plots/ss_v_ctrl/')
  parser.add_argument('-f','--force', dest = 'force', action = 'store_true', default = False,
                      help = 'Force overwrite')
  args = parser.parse_args()
  main(args)