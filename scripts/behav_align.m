force = true; % CHANGE

for idx = 1:12
id = num2str(idx,'%02u');
f = readlines(['/cephfs/zlatic_lab/code/yhe/params/drivers#',id,'.list']);
f = f(strlength(f)>0);
addpath /cephfs/zlatic_lab/code/yhe/scripts;
tic;

behavs = ["run","cast","stop","hunch","back","roll","other"];
time = int32(-15000:100/3:29967);
aligned = struct();
for j = 1:length(behavs)
    aligned.(behavs(j)) = table(time', 'VariableNames',"time");
end
neff = zeros(length(f),1);
nmax = zeros(length(f),1);

for i = 1:length(f)
    gen = f(i);
    disp(gen);
    disp(num2str(i) + "/" + num2str(length(f)));
    out_fname = "/cephfs/zlatic_lab/code/yhe/behav/"+gen+"_raw.txt";
    out = plot_genotype_trx(gen,force);
    tmp1 = out.raw(15000<=out.raw.time & out.raw.time < 60000,:);
    tmp2 = out.raw(60000<=out.raw.time & out.raw.time <105000,:);
    tmp1.time = tmp1.time - 30000;
    tmp2.time = tmp2.time - 75000;
    merged = join(tmp1,tmp2,'Keys','time');
    writetable(merged,out_fname);
    out_fname = "/cephfs/zlatic_lab/code/yhe/behav/"+gen+".txt";
    writetable(out.aligned, out_fname);
    for j = 1:length(behavs)
        aligned.(behavs(j)).(gen) = out.aligned.("p"+behavs(j));
    end
    toc;
end

for j = 1:length(behavs)
    out_fname = "/cephfs/zlatic_lab/code/yhe/behav/"+behavs(j)+"_summary_#"+id+".txt";
    writetable(aligned.(behavs(j)), out_fname);
end
end