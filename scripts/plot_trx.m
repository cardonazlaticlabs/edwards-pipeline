function out = plot_trx(trx_file, force)
    behavs = ["run","cast","stop","hunch","back","roll","other"];
    colororder({'#fbb4ae','#b3cde3','#ccebc5','#decbe4','#fed9a6','#ffffcc','#e5d8bd'}) 
    % set2 on Colour Brewer https://colorbrewer2.org
    load(trx_file,'trx');
    tbl = table(trx(1).t, 'VariableNames',"time");
    for i = 1:length(trx)
        tmp = trx(i);
        t = tmp.t;
        behav = repmat("other",length(t),1);
%         behav(tmp.run==1) = "run";
%         behav(tmp.cast==1) = "cast";
%         behav(tmp.stop==1) = "stop";
%         behav(tmp.hunch==1) = "hunch";
%         behav(tmp.back==1) = "back";
%         behav(tmp.roll==1) = "roll";
        behav(tmp.run_large==1) = "run";
        behav(tmp.cast_large==1) = "cast";
        behav(tmp.stop_large==1) = "stop";
        behav(tmp.hunch_large==1) = "hunch";
        behav(tmp.back_large==1) = "back";
        behav(tmp.roll_large==1) = "roll";
        behav = categorical(behav);
        changes(i) = record_changes(t, behav);
        tmptbl = table(t,behav);
        tmptbl.Properties.VariableNames = ["time","id_"+tmp.numero_larva];
        tbl = outerjoin(tbl,tmptbl,'Keys','time','MergeKeys',true);
    end
    colnames = tbl.Properties.VariableNames;
    for i = 2:size(tbl,2)
        tbl(:,i) = array2table(apply_changes(tbl.time, changes(i-1)));
    end
    tbl.Properties.VariableNames = colnames;
    tmp = table2array(tbl(:,2:end));
    summary_tbl = tbl(:,1);
    for b = behavs
        summary_tbl.("p"+b) = sum(tmp==b,2)./(size(tmp,2)-sum(isundefined(tmp),2));
    end
    if ~isfile([trx(1).full_path,'_summary.pdf']) || force
        figure('OuterPosition',[0,0,1920,1080]);
        area(summary_tbl.time, table2array(summary_tbl(:,2:end)));
        colororder({'#fbb4ae','#b3cde3','#ccebc5','#decbe4','#fed9a6','#ffffcc','#e5d8bd'});
        legend(behavs,'Location','eastoutside');
        ylim([0,1]);
        title(replace(trx(1).neuron,'_','\_'));
        exportgraphics(gca,[trx(1).full_path,'_summary.pdf']);
        delete(gcf);
    end
    out = struct('raw', tbl, 'summary', summary_tbl, ...
        'changes', changes, 'time', tbl.time);
end

function changes = record_changes(t, behav)
    changes = struct;
    tout = t(1);
    behav_out = behav(1);
    for i = 2:length(t)-1
        if behav(i) ~= behav_out(end)
            tout = [tout; t(i)]; behav_out = [behav_out; behav(i)];
        end
    end
    tout = [tout;t(end)]; behav_out = [behav_out;behav(end)];
    changes.time = tout; changes.behav = behav_out;
end

function ts = apply_changes(t, changes)
    ts = categorical(nan(length(t),1));
    for i = 1:length(changes.time)-1
        ts(t>=changes.time(i) & t< changes.time(i+1)) = changes.behav(i);
    end
    ts(t==changes.time(end)) = changes.behav(end);
end