import os
from fnmatch import fnmatch
from time import perf_counter as t
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-f','--force', dest = 'force', default = False, action = 'store_true',
                    help = 'force overwrite')
args = parser.parse_args()
if args.force: fc = '-f'
else: fc = ''
os.chdir('/cephfs/zlatic_lab/code/yhe/params/')
tic = t()
for i in range(1,12):
  f = f'drivers#{i:02.0f}.list'
  f = open(f).read().splitlines()
  for j in f:
    if fnmatch(j,'empty*'):
      ctrl = j
      f.remove(j)
  ctrl = f'/cephfs/zlatic_lab/code/yhe/behav/{ctrl}.txt'
  for j in f:
    test = f'/cephfs/zlatic_lab/code/yhe/behav/{j}.txt'
    os.system(f'sbatch -c 4 -o /dev/null /cephfs/zlatic_lab/code/yhe/scripts/pymaster.sh /cephfs/zlatic_lab/code/yhe/scripts/behav_stats.py --ctrl {ctrl} --test {test} {fc}')
    toc = t() - tic
    print(f'Processed: {j}. Time = {toc:.3f} seconds.')