function out = plot_genotype_trx(gen, force, wd, rig)
    arguments
        gen string
        force = false   
        wd string = "/cephfs/zlatic_lab/pipeline/screen/behaviour-tagging-results/"
        rig string = "BO1"
    end
    
    addpath /cephfs/zlatic_lab/code/yhe/scripts;
    cd(wd);
    cd(rig);
    d = dir(gen+"*");
    if length(d) > 1; error('selected genotype is not unique!'); end
    cd(d.name);
    if isfile(gen+".mat") && /cephfs/zlatic_lab/code/yheforce; load(gen+".mat",'out'); return; end

    full_geno = d.name;
    protocols = split(string(ls()));
    protocols = protocols(strlength(protocols)>0);
    protocols = protocols(protocols ~= gen+".mat");
    for prot = 1:length(protocols)
        oldfolder = cd(protocols(prot));
        % initialise output
        changes = struct.empty;
        
        % t = [];
        runs = split(string(ls()));
        runs = runs(isfolder(runs));
        for run = 1:length(runs)
            run_info = plot_trx(runs(run)+"/trx.mat", force);
            changes = [changes,run_info.changes];
            % t = union(t, int32(1000*run_info.time),'sorted');
        end

        % for alignment between the 1st and 2nd stimuli
%         t = unique(t);
%         tmp1 = t(t>=15000 & t<60000) + 45000;
%         tmp2 = t(t>=60000 & t<105000)- 45000;
%         t = union(t,tmp1, 'sorted');
%         t = union(t,tmp2, 'sorted');
%         t = unique(t);
        
        t = int32(100/3:100/3:120000)';
        master_tbl = table(t,'VariableNames',"time");
        for i = 1:length(changes)
            master_tbl.("id_"+string(i)) = ...
                apply_changes(master_tbl.time, changes(i));
        end
        
        % summarise to percentage
        behavs = ["run","cast","stop","hunch","back","roll","other"];
        tmp = table2array(master_tbl(:,2:end));
        summary_tbl = master_tbl(:,1);
        for i = 1:length(behavs)
            b = behavs(i);
            summary_tbl.("p"+b) = sum(tmp==b,2)./(size(tmp,2)-sum(isundefined(tmp),2));
        end
        summary_tbl.n = sum(~isundefined(tmp),2);
        

        tmp1 = summary_tbl(summary_tbl.time>=15000 & summary_tbl.time<60000,:);
        t_aligned = tmp1.time - 30000;
        tmp2 = summary_tbl(summary_tbl.time>=60000 & summary_tbl.time<105000,:);
        tmp1.time = tmp1.time-30000;
        tmp2.time = tmp2.time - 75000;
        aligned_tbl = table(t_aligned, 'VariableNames', "time");
        for i = 1:length(behavs)
            b = behavs(i);
            fn = "p" + b;
            aligned_tbl.(fn) = (tmp1.(fn).*tmp1.n + tmp2.(fn) .*tmp2.n)./(tmp1.n+tmp2.n);
        end
        aligned_tbl.n = tmp1.n+tmp2.n;

        % plot
        if ~isfile(gen+'_genotype_summary.pdf') || force
            figure('OuterPosition',[0,0,1920,1080]);
            area(summary_tbl.time, table2array(summary_tbl(:,2:end)));
            colororder({'#fbb4ae','#b3cde3','#ccebc5','#decbe4','#fed9a6','#ffffcc','#e5d8bd'});
            legend(behavs,'Location','eastoutside');
            ttl = replace(full_geno,'_','\_') + ", neff="+...
                num2str(mean(summary_tbl.n),'%.2f')+", nmax="+...
                num2str(max(summary_tbl.n),'%.0f');
            title(ttl);
            ylim([0,1]);
            fontsize(20,'points');
            exportgraphics(gca,gen+'_genotype_summary.pdf');
            delete(gcf);
        end
        
        if ~isfile(gen+'_genotype_aligned.pdf') || force
            figure('OuterPosition',[0,0,1920,1080]);
            area(aligned_tbl.time, table2array(aligned_tbl(:,2:end)));
            colororder({'#fbb4ae','#b3cde3','#ccebc5','#decbe4','#fed9a6','#ffffcc','#e5d8bd'});
            legend(behavs,'Location','eastoutside');
            ttl = replace(full_geno,'_','\_') + ", neff="+...
                num2str(mean(summary_tbl.n),'%.2f')+", nmax="+...
                num2str(max(summary_tbl.n),'%.0f');
            title(ttl);
            ylim([0,1]);
            fontsize(20,'points');
            exportgraphics(gca,gen+'_genotype_aligned.pdf');
            delete(gcf);
        end

        cd(oldfolder);
        out(prot) = struct('raw',master_tbl,'summary',summary_tbl, ...
            'aligned',aligned_tbl,'protocol',protocols(prot));
    end
    save(gen+".mat",'out');
end

function ts = apply_changes(t, changes)
    ts = categorical(nan(length(t),1));
    for i = 1:length(changes.time)-1
        ts(t>=1000*changes.time(i) & t<1000*changes.time(i+1)) = changes.behav(i);
    end
    ts(abs(t-1000*changes.time(end))<10) = changes.behav(end);
end