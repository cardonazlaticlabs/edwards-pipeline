#!/cephfs/zlatic_lab/code/yhe/anaconda3/bin python3
# -*- coding: utf-8 -*-

def main(args):
  # args.ctrl: file for ctrl ts
  # args.test: file for test ts
  # args.out: out dir
  # args.force: force overwrite
  
  import os
  if not os.path.isdir(args.out): os.mkdir(args.out)
  out_fname = args.out + os.path.basename(args.test)
  import numpy as np
  import pandas as pd
  import scipy.stats as sts
  
  thres95 = sts.chi2.ppf(.95,df = 6)
  thres999 = sts.chi2.ppf(.999,df = 6)
  
  if os.path.isfile(out_fname) and not args.force:
    out = pd.read_csv(out_fname)
    nt = out.shape[0]
  else:
    ctrl = pd.read_csv(args.ctrl, sep = ',') # time = col0, each subsequent column = individual
    test = pd.read_csv(args.test, sep = ',')
    nt = ctrl.shape[0]
    
    out = pd.DataFrame(dict(time = ctrl.time))
    out['n'] = test.n
    ctrl = ctrl.iloc[:,1:]
    test = test.iloc[:,1:]
    
    chi2 = np.zeros(nt)
    pchi2 = np.zeros(nt)
    dev = np.zeros(nt)
    gstat = np.zeros(nt)
    pg = np.zeros(nt)
    
    for i in range(nt):
      chi2[i],pchi2[i] = chi2_pseudo(ctrl.iloc[i,:], test.iloc[i,:])
      dev[i] = deviance(ctrl.iloc[i,:], test.iloc[i,:])
      gstat[i],pg[i] = gstat_pseudo(ctrl.iloc[i,:], test.iloc[i,:])
      
    # 10000 permutations of gstats
    _, cm_gstat_95 = cluster_mass(gstat, thres95)
    cm_gstat_95 = np.array(cm_gstat_95)
    _, cm_gstat_999 = cluster_mass(gstat, thres999)
    cm_gstat_999 = np.array(cm_gstat_999)
    rng = np.random.Generator(np.random.MT19937(seed = 530617))
    null_dist_95 = []
    null_dist_999 = []
    for i in range(args.nperm):
      tmp, _ = cluster_mass(rng.permutation(gstat), thres95)
      null_dist_95.append(max(tmp))
      tmp, _ = cluster_mass(rng.permutation(gstat), thres999)
      null_dist_999.append(max(tmp))
    null_dist_95.sort()
    null_dist_95 = np.array(null_dist_95)
    thr_cluster_mass_95 = null_dist_95[int(args.nperm*0.99)]
    null_dist_999.sort()
    null_dist_999 = np.array(null_dist_999)
    thr_cluster_mass_999 = null_dist_999[int(args.nperm*0.99)]
    
    # filter95 = cm_gstat_95<=thr_cluster_mass_95
    # filter95_c = filter95.copy()
    # for i in range(len(filter95)):
    #   if not (filter95_c[i-1] and filter95_c[i+1]): # take the points on both sides of the cluster as well
    #     filter95[i] = False
    # filter999 = cm_gstat_999<=thr_cluster_mass_999
    # filter999_c = filter999.copy()
    # for i in range(len(filter999)):
    #   if not (filter999_c[i-1] and filter999_c[i+1]): # take the points on both sides of the cluster as well
    #     filter999[i] = False
    sig_g_clusters_95 = gstat.copy()
    # sig_g_clusters_95[filter95] = np.nan # non-sig clusters
    sig_g_clusters_95[cm_gstat_95<=thr_cluster_mass_95] = np.nan
    sig_g_clusters_999 = gstat.copy()
    sig_g_clusters_999[cm_gstat_999<=thr_cluster_mass_999] = np.nan
    # sig_g_clusters_999[filter999] = np.nan
    
    out['chi2'] = chi2
    out['pchi2'] = pchi2
    out['gstat'] = gstat
    out['pg'] = pg
    out['dev'] = dev
    out['sig_clusters_.05'] = sig_g_clusters_95
    out['sig_clusters_.001'] = sig_g_clusters_999
    out.to_csv(out_fname, index = False)
  
  out_fig = args.out + os.path.basename(args.test).replace('txt', 'pdf')
  
  tmp = out[['time','gstat','sig_clusters_.05','sig_clusters_.001']]
  import matplotlib.pyplot as plt
  _, ax = plt.subplots(figsize = (14,6))
  ax.plot(tmp.time, tmp.gstat, label = 'G statistic')
  ax.plot(tmp.time,tmp['sig_clusters_.05'], label = 'Sig clusters .05')
  ax.plot(tmp.time,tmp['sig_clusters_.001'], label = 'Sig clusters .001')
  plt.title(os.path.basename(args.test).replace('.txt', ''))
  plt.ylim([-5,100])
  plt.ylabel('G statistic / chi2')
  plt.xlabel('time (ms)')
  plt.axhline(thres95, color = 'b', linestyle = '--', label = 'chi2(p=0.95, df=6)')
  plt.axhline(thres999, color = 'r', linestyle = '--', label = 'chi2(p=0.999, df=6)')
  plt.axvspan(0, 15000, fc = '#dfdfdf', color = 'k')
  plt.legend()
  plt.savefig(out_fig)
  plt.close()
  return 0
  
def chi2_pseudo(ctrl, test):
  # last element of test must be n
  import numpy as np
  import scipy.stats as sts
  df = len(ctrl) - 2
  n = test[-1]
  c = ctrl[:-1]+.001
  c /= c.sum()
  t = (c-test[:-1])**2 / c * n
  t = np.sum(t)
  p = 1-sts.chi2.cdf(t, df = df)
  return t,p

def deviance(ctrl,test):
  # last element of both must be n
  c = ctrl[:-1]
  t = test[:-1]
  return(sum((c-t)**2))

def gstat_pseudo(ctrl,test):
  import numpy as np
  import scipy.stats as sts 
  n = test[-1]
  df = len(ctrl) - 2
  c = ctrl[:-1]+.001
  c /= c.sum()
  t = 2*n*test[:-1]*np.log(test[:-1]/c)
  t = np.sum(t)
  p = 1-sts.chi2.cdf(t, df = df)
  return t,p

def cluster_mass(ts, thres):
  # ts is an 1d array, threshold is a float
  cm = []
  cms = []
  current = 0
  currentn = 0
  for x in ts:
    if x > thres:
      current += x
      currentn += 1
    elif current != 0:
      cm.append(current)
      for i in range(currentn):
        cms.append(current)
      cms.append(0)
      current = 0
      currentn = 0
    else: cms.append(0)
  if current != 0:
    cm.append(current)
    for i in range(currentn):
      cms.append(current)
  return cm, cms

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser(description = 'Cliffs Delta calculator for behavioural sequences')
  parser.add_argument('--ctrl', dest = 'ctrl', help = 'Control line')
  parser.add_argument('--test', dest = 'test', help = 'Test line')
  parser.add_argument('-n','--nperm', dest = 'nperm', help = '# permutations',
                      type = int, default = 50000)
  parser.add_argument('-o','--out', dest = 'out', help = 'Output dir', default = '/cephfs/zlatic_lab/code/yhe/stats/')
  parser.add_argument('-f','--force', dest = 'force', action = 'store_true', default = False,
                      help = 'Force overwrite')
  args = parser.parse_args()
  main(args)